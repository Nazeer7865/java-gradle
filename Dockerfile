FROM openjdk
COPY /home/gitlab-runner/*.jar /
EXPOSE 8080
ENTRYPOINT ["java","-jar","/java-gradle.jar"]
